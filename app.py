from flask import Flask, jsonify
import numpy as np

app = Flask(__name__)

def numerical_integration(func, lower, upper, N):
    width = (upper - lower) / N
    total_area = 0
    for i in range(N):
        height = func(lower + i * width)
        total_area += height * width
    return total_area

@app.route('/numericalintegralservice/<lower>/<upper>')
def integrate(lower, upper):
    results = {}
    for N in [10, 100, 1000, 10000, 100000, 1000000]:
        result = numerical_integration(lambda x: np.abs(np.sin(x)), float(lower), float(upper), N)
        results[N] = result
    return jsonify(results)

if __name__ == '__main__':
    app.run(debug=True)

